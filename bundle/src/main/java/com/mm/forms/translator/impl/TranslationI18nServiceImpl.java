package com.mm.forms.translator.impl;

import java.util.Locale;
import java.util.ResourceBundle;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.i18n.ResourceBundleProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.i18n.I18n;
import com.mm.forms.translator.TranslationI18nService;

//import java.util.LinkedList;
//import java.util.List;
//import com.google.gson.Gson;
//import com.mm.forms.translator.MmPostProcessor;

@Component(immediate = true, metatype = true, label = "Translation i18n Service")
@Service(value = TranslationI18nService.class)
public class TranslationI18nServiceImpl implements TranslationI18nService {

   //private static final Logger LOG = LoggerFactory.getLogger(TranslationI18nService.class);

    @Reference(target="(component.name=org.apache.sling.i18n.impl.JcrResourceBundleProvider)") 
    private ResourceBundleProvider resourceBundleProvider;
    
	public String get(String basename, String language, String text) {
		
        Locale locale = new Locale(language);
      
        ResourceBundle resourceBundle = resourceBundleProvider.getResourceBundle(basename, locale);    
        I18n i18n = new I18n(resourceBundle);   

        String translatedText = i18n.get(text);
        
        return translatedText;

        /*
        List<String> texts = new LinkedList<String>();
        String translatedText = i18n.get(text);
        texts.add(translatedText);
        Gson gson = new Gson();
        String responseData = gson.toJson(texts);
        return responseData;
         */
	}

}
