package com.mm.forms.translator;

public interface TranslationI18nService {
	String get(String basename, String language, String text);
}
