package com.mm.forms.translator;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.commons.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.aem.dermis.api.processor.IPostProcessor;
import com.adobe.aem.dermis.api.processor.IProcessor;
import com.adobe.aem.dermis.exception.DermisException;
import com.adobe.aem.dermis.model.ExecutionContext;
import com.adobe.aem.dermis.model.value.IValue;
import com.google.gson.Gson;

import com.adobe.aem.dermis.util.ValueUtil;

@Component(immediate = true, enabled = false)
@Service(IProcessor.class)
public class MmPostProcessor implements IPostProcessor {

   private static final Logger LOG = LoggerFactory.getLogger(MmPostProcessor.class);

   @Reference TranslationI18nService translationI18nService;
   
   public IValue postProcess(ExecutionContext arg0, IValue arg1) throws DermisException {

       LOG.info("Postprocessing requested");
       LOG.info("Execution Context=" + arg0);
       //LOG.info("IValue=" + arg1);
       LOG.info("IValue json=" + arg1.toJson()); 

       LOG.info("arg0.toString()=" + arg0.toString());
       LOG.info("arg0.getArguments()=" + arg0.getArguments());
       LOG.info("arg0.getSchemaName()=" + arg0.getSchemaName());
       LOG.info("arg0.getQuery()=" + arg0.getQuery());
              
       String basename = "/content/forms/af/mclaren/enquire-230221-moris-pers/jcr:content/guideContainer/assets/dictionary";
       String language = "fr";
       String translatedText = null;
       String valueStr = arg1.toString();
       
       String[] arrOfStr = valueStr.split(" ntt_language=", 0); 
       
       StringBuffer sb = new StringBuffer();
       sb.append(arrOfStr[0]);
       
       for (int i = 1; i < arrOfStr.length; i++) {
    	   String[] textlang = arrOfStr[i].split(",",2);
    	   translatedText = translationI18nService.get(basename, language, textlang[0]);
    	   String newText = " ntt_language=" + translatedText + "," + textlang[1];
    	   sb.append(newText);
       };
       
       String sbString = sb.toString();
       LOG.info("***** sbString = ");
   
/*
       Gson gson = new Gson();
       String responseData = gson.toJson(sbString);
       LOG.info("***** responseData = ");
       
       IValue newIValue = arg1;
      try {
		newIValue = ValueUtil.jsonToValue(responseData);
		LOG.info("***** newIValue = ");
	} catch (DermisException e) {
		// TODO Auto-generated catch block
		LOG.info("***** DermisException");
		e.printStackTrace();
	} catch (JSONException e) {
		// TODO Auto-generated catch block
		LOG.info("***** JSONException");
		e.printStackTrace();
	}

       // Return the updated IValue
	   return newIValue;
*/
       return arg1;
	}

   public boolean isSupported(String connectorName, ExecutionContext executionContext) {
	   LOG.info("Checking if the postprocessing is supported");
	   try {
	       LOG.info("isSupported Connector Name received=" + connectorName);
	       LOG.info("isSupported Execution Context=" + executionContext);
	   } catch (Exception e) {
	       LOG.error(e.getMessage());
	   }
	   return true;
   }

}